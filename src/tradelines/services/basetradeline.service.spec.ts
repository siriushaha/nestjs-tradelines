import { Test, TestingModule } from '@nestjs/testing';
import { ITradeline, ILineInfo, IExpenses, ISummary } from '../interfaces/tradelines.interface';
import { BaseTradelineService } from './basetradeline.service';

describe('Base Tradeline.parseTradeline', () => {
  let baseTradeline;
  beforeEach(() => {
    baseTradeline = new BaseTradelineService();
  });

  it("should return valid tradeline", () => {
    const tradeline =  '2015-10-10 10 12 $1470.31 $659218.00';
    const tradeLine = baseTradeline.parseTradeline(tradeline);

    expect(tradeLine.reportedDate).toBe('2015-10-10');
    expect(tradeLine.code).toBe(10);
    expect(tradeLine.subcode).toBe(12);
    expect(tradeLine.monthlyPayment).toBe(147031);
    expect(tradeLine.currentBalance).toBe(65921800);
  });

  it("should ignore tradelines with unexpected number of entries", () => {
    const tradeline1 = '2015-10-10 10 12';
    const tradeline2 = '2015-10-10 10 12 $1470.31 $659218.00 23';
    const tradeLine1 = baseTradeline.parseTradeline(tradeline1);
    const tradeLine2 = baseTradeline.parseTradeline(tradeline2);

    expect(tradeLine1).toBe(null);
    expect(tradeLine2).toBe(null);
  });

  it("should ignore tradeline prior to filtered date", () => {
    const tradeline = '2015-10-10 10 12 $1470.31 $659218.00';
    const tradeLine = baseTradeline.parseTradeline(tradeline, '2015-10-11');

    expect(tradeLine).toBe(null);
  });

  it("should return valid tradeline after filtered date", () => {
    const tradeline =  '2015-10-10 10 12 $1470.31 $659218.00';
    const tradeLine = baseTradeline.parseTradeline(tradeline, '2015-10-10');

    expect(tradeLine.reportedDate).toBe('2015-10-10');
    expect(tradeLine.code).toBe(10);
    expect(tradeLine.subcode).toBe(12);
    expect(tradeLine.monthlyPayment).toBe(147031);
    expect(tradeLine.currentBalance).toBe(65921800);
  });


  it("should return tradeline with valid monthly payment and current balance", () => {
    const tradeline = '2015-10-10 10 12 $1,470.31 $659,218.00';
    const tradeLine = baseTradeline.parseTradeline(tradeline);

    expect(tradeLine.monthlyPayment).toBe(147031);
    expect(tradeLine.currentBalance).toBe(65921800);
  });

});



describe('Base Tradeline.transformTradeline', () => {
  let baseTradeline;
  beforeEach(() => {
    baseTradeline = new BaseTradelineService();
  });

  it("should return valid mortgage tradelines", () => {
    const tradeLine: ITradeline = {
      reportedDate: '2015-10-10',
      code: 5,
      subcode: 12,
      monthlyPayment: 14703,
      currentBalance: 659218,
    };
    const tradeLineInfo: ILineInfo = {
      tradeline_type: 'education',
      monthly_payment: 14703,
      current_balance: 659218,
    };

    expect(baseTradeline.transformTradeline(tradeLine)).toEqual(tradeLineInfo);

  });

  it("should return valid education tradelines", () => {
    const tradeLine1: ITradeline = {
      reportedDate: '2015-10-10',
      code: 10,
      subcode: 12,
      monthlyPayment: 147031,
      currentBalance: 65921800,
    };
    const tradeLine2: ITradeline = {
      reportedDate: '2015-10-10',
      code: 10,
      subcode: 15,
      monthlyPayment: 147031,
      currentBalance: 65921800,
    };
    const tradeLineInfo1: ILineInfo = {
      tradeline_type: 'mortgage',
      monthly_payment: 147031,
      current_balance: 65921800,
    };
    const tradeLineInfo2: ILineInfo = {
      tradeline_type: 'mortgage',
      monthly_payment: 147031,
      current_balance: 65921800,
    };

    expect(baseTradeline.transformTradeline(tradeLine1)).toEqual(tradeLineInfo1);
    expect(baseTradeline.transformTradeline(tradeLine2)).toEqual(tradeLineInfo2);
  });

  it("should return tradelines other than mortgage or education tradeline", () => {
    const tradeLine1: ITradeline = {
      reportedDate: '2015-10-10',
      code: 12,
      subcode: 12,
      monthlyPayment: 147031,
      currentBalance: 65921800,
    };
    const tradeLine2: ITradeline = {
      reportedDate: '2015-10-10',
      code: 23,
      subcode: 15,
      monthlyPayment: 147031,
      currentBalance: 65921800,
    };
    const tradeLineInfo1: ILineInfo = {
      tradeline_type: 'other',
      monthly_payment: 147031,
      current_balance: 65921800,
    };
    const tradeLineInfo2: ILineInfo = {
      tradeline_type: 'other',
      monthly_payment: 147031,
      current_balance: 65921800,
    };

    expect(baseTradeline.transformTradeline(tradeLine1)).toEqual(tradeLineInfo1);
    expect(baseTradeline.transformTradeline(tradeLine2)).toEqual(tradeLineInfo2);
  });

});



