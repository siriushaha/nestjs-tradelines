import { Controller, Post, Body, Param } from '@nestjs/common';
import { TradelinesService } from '../services/tradelines.service';
import { ITradelineSummary } from '../interfaces/tradelines.interface';

@Controller('tradelines')
export class TradelinesController {
  constructor(private readonly tradelineService: TradelinesService) {}

  @Post()
  createTradelineSummary(@Body() tradelines: string[], @Param('filteredDate') filteredDate: string = null): string {
    const summary = this.tradelineService.summarizeTradelinesFromCreditReport(tradelines, filteredDate);
    const tradelineSummary: ITradelineSummary = {
      fixed_expenses_before_education: summary.expenses.fixed_expenses_before_education,
      tradelines: summary.tradelines,
    };
    return JSON.stringify(tradelineSummary, null, 2).replace('tradeline_type', 'type');
  }
}
