<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest
  
  <p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>
    <p align="center">

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

This implementation based on Nest.js provides REST POST API to provide a summary of fixed expenses before education and list of tradelines from user's credit report.
```
POST API with url at http://host:3000/tradelines

Body of request with tradelines from user's credit report as:
[
  "2015-10-10 10 12 $1470.31 $659218.00",
  "2015-10-10 5 1 $431.98 $51028.00",
  "2015-10-09 8 15 $340.12 $21223.20",
  "2015-10-10 10 15 $930.12 $120413.00",
  "2015-10-09 12 5 $150.50 $6421.21"
]

Response from POST API as:
{
  "fixed_expenses_before_education": 289105,
  "tradelines": [
    {
      "tradeline_type": "mortgage",
      "monthly_payment": 147031,
      "current_balance": 65921800
    },
    {
      "tradeline_type": "education",
      "monthly_payment": 43198,
      "current_balance": 5102800
    },
    {
      "tradeline_type": "other",
      "monthly_payment": 34012,
      "current_balance": 2122320
    },
    {
      "tradeline_type": "mortgage",
      "monthly_payment": 93012,
      "current_balance": 12041300
    },
    {
      "tradeline_type": "other",
      "monthly_payment": 15050,
      "current_balance": 642121
    }
  ]
}
```
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
## Daniel Ha

## License

  Nest is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).

