import {
  ILineInfo,
  ITradeline,
  IExpenses,
  ISummary,
} from '../interfaces/tradelines.interface';
import * as moment from 'moment';

export class BaseTradelineService {
  /**
   * Calculate fixed expenses before Education based on housing and non-housing expenses
   * Housing expenses is the sum of monthly payments for all mortgage tradelines. If credit report has no mortgage tradelines, set $1061 as default
   * Non-housing expense is the sum of monthly payments for tradelines which are not mortgage and education ie student loan
   *
   * @param lineInfos
   * @returns IExpenses
   */
  protected getFixedExpensesBeforeEducation(lineInfos: ILineInfo[]): IExpenses {
    let mortgageCount = 0;
    let housingExpenses = 0;
    let nonHousingExpenses = 0;
    for (const lineInfo of lineInfos) {
      // Ignore line for calculation if there is no current balance ie paid account
      if (lineInfo.current_balance === 0) {
        continue;
      }

      switch (lineInfo.tradeline_type) {
        // calculate Housing Expense
        case 'mortgage':
          housingExpenses += lineInfo.monthly_payment;
          mortgageCount++;
          break;

        // calculate non housing expense for lines which are not mortgages or student loans
        case 'other':
          nonHousingExpenses += lineInfo.monthly_payment;
          break;

        default:
          break;
      }
    }

    // Default housing expenses to national avarage monthly rent if there is no mortgage
    if (mortgageCount === 0) {
      housingExpenses = 106100;
    }
    return {
      fixed_expenses_before_education: housingExpenses + nonHousingExpenses,
      housing_expenses: housingExpenses,
      non_housing_expenses: nonHousingExpenses,
    };
  }

  /**
   * Transform ITradeline tradeline into ILineInfo format:
   * Set type to 'mortgage' if code is 10 and subcode is 12 or 15,
   * Set type to 'education' if code is 5
   * Set type to 'other' if code is 12 or any code other than 10 or 5
   * @param tradeLine
   * @returns ILineInfo
   */
  protected transformTradeline(tradeLine: ITradeline): ILineInfo {
    const getTradelineType = () => {
      let tradelineType: string;
      switch (tradeLine.code) {
        case 10:
          tradelineType =
            tradeLine.subcode === 12 || tradeLine.subcode === 15
              ? 'mortgage'
              : 'invalid mortgage subcode';
          break;

        case 5:
          tradelineType = 'education';
          break;

        case 12:
        default:
          tradelineType = 'other';
          break;
      }
      return tradelineType;
    };

    const lineInfo: ILineInfo = {
      tradeline_type: getTradelineType(),
      monthly_payment: tradeLine.monthlyPayment,
      current_balance: tradeLine.currentBalance || 0,
    };

    return lineInfo;
  }

  /**
   * Parse tradeline in credit report format with following order:
   * reported date, code, subcode, monthly payment and current balance.
   * Monthly payment and current balance in monetary values might contain symbols $, comma and period to be removed
   * @param tradeline
   * @returns ITradeline
   */
  protected parseTradeline(tradeline: string, filteredDate: string = null): ITradeline {
    const parseCurrencyField = (field: string): number =>
      +field.replace(/\$|\,|\./gi, '');
    const tokens = tradeline.split(' ');
    const count = tokens.length;
    // Ignore line with unexpected number of fields
    if (count < 4 || count > 5) {
      return null;
    }
    if (filteredDate) {
      const lineDateMS = new Date(tokens[0]).valueOf();
      const filteredDateMS = new Date(filteredDate).valueOf();
      // console.log(lineDateMS, filteredDateMS)
      if (lineDateMS < filteredDateMS) {
        return null;
      }
    }

    // Proceed to parse line with correct number of fields
    const tradeLine: ITradeline = {};
    for (let i = 0; i < count; i++) {
      switch (i) {
        case 0:
          tradeLine.reportedDate = tokens[0];
          break;

        case 1:
          tradeLine.code = +tokens[1];
          break;

        case 2:
          tradeLine.subcode = +tokens[2];
          break;

        case 3:
          tradeLine.monthlyPayment = parseCurrencyField(tokens[3]);
          break;

        case 4:
          tradeLine.currentBalance = parseCurrencyField(tokens[4]);
          break;

        default:
          break;
      }
    }
    // console.log(tradeLine);
    return tradeLine;
  }
}
