import { Test, TestingModule } from '@nestjs/testing';
import { TradelinesService } from './tradelines.service';
import { ISummary, ITradelineSummary } from '../interfaces/tradelines.interface';

describe(TradelinesService, () => {
  let service: TradelinesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TradelinesService],
    }).compile();
    service = module.get<TradelinesService>(TradelinesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return valid tradeline summary', () => {
    const tradelines = [
      '2015-10-10 10 12 $1470.31 $659218.00',
      '2015-10-10 5 1 $431.98 $51028.00',
      '2015-12-09 8 15 $340.12 $21223.20',
      '2015-10-10 10 15 $930.12 $120413.00',
      '2015-10-09 12 5 $150.50 $6421.21',
    ];
    const summary: ISummary = service.summarizeTradelinesFromCreditReport(
      tradelines
    );
    // console.log('******', summary.tradelines)
    expect(summary.expenses.fixed_expenses_before_education).toBe(289105);
    expect(summary.expenses.housing_expenses).toBe(240043);
    expect(summary.expenses.non_housing_expenses).toBe(49062);
    expect(summary.tradelines.length).toBe(5);
  });

  it('should return valid tradeline summary including default housing expense', () => {
    const tradelines = [
      '2015-10-10 5 1 $431.98 $51028.00',
      '2015-10-09 8 15 $340.12 $21223.20',
      '2015-10-09 12 5 $150.50 $6421.21',
    ];
    const summary: ISummary = service.summarizeTradelinesFromCreditReport(
      tradelines
    );

    expect(summary.expenses.fixed_expenses_before_education).toBe(155162);
    expect(summary.expenses.housing_expenses).toBe(106100);
    expect(summary.expenses.non_housing_expenses).toBe(49062);
    expect(summary.tradelines.length).toBe(3);
  });

  it('should return "tradeline summary including invalid mortgage subcode"', () => {
    const tradelines = [
      '2015-10-10 10 12 $1470.31 $659218.00',
      '2015-10-10 5 1 $431.98 $51028.00',
      '2015-10-09 8 15 $340.12 $21223.20',
      '2015-10-10 10 15 $930.12 $120413.00',
      '2015-10-09 12 5 $150.50 $6421.21',
      '2015-10-09 10 5 $150.50 $6421.21' ,
    ];
    const summary: ISummary = service.summarizeTradelinesFromCreditReport(
      tradelines
    );
    // console.log(summary);
    expect(summary.expenses.fixed_expenses_before_education).toBe(289105);
    expect(summary.expenses.housing_expenses).toBe(240043);
    expect(summary.expenses.non_housing_expenses).toBe(49062);
    expect(summary.tradelines.length).toBe(6);
  });

  it('should return "tradeline summary to exclude tradelines with incorrect number of entries"', () => {
    const tradelines = [
      '2015-10-10 10 12 $1470.31 $659218.00',
      '2015-10-10 5 1 $431.98 $51028.00',
      '2015-10-09 8 15 $340.12 $21223.20',
      '2015-10-10 10 15 $930.12 $120413.00',
      '2015-10-09 12 5 $150.50 $6421.21',
      '2015-10-09 10 5',
      '2015-10-10 5 1 $431.98 $51028.00 340',
    ];
    const summary: ISummary = service.summarizeTradelinesFromCreditReport(
      tradelines
    );

    // console.log(summary);
    expect(summary.expenses.fixed_expenses_before_education).toBe(289105);
    expect(summary.expenses.housing_expenses).toBe(240043);
    expect(summary.expenses.non_housing_expenses).toBe(49062);
    expect(summary.tradelines.length).toBe(5);
  });

});
