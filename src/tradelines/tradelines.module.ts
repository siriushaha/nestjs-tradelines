import { Module } from '@nestjs/common';
import { TradelinesController } from './controllers/tradelines.controller';
import { TradelinesService } from './services/tradelines.service';

@Module({
  imports: [],
  controllers: [TradelinesController],
  providers: [TradelinesService],
})
export class TradelinesModule {}
