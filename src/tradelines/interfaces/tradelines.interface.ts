export interface ITradeline {
  reportedDate?: string;
  code?: number;
  subcode?: number;
  monthlyPayment?: number;
  currentBalance?: number;
}

export interface ILineInfo {
  tradeline_type: string;
  monthly_payment: number;
  current_balance: number;
}

export interface IExpenses {
  fixed_expenses_before_education: number;
  housing_expenses: number;
  non_housing_expenses: number;
}

export interface ISummary {
  expenses: IExpenses;
  tradelines: ILineInfo[];
}
export interface ITradelineSummary {
  fixed_expenses_before_education: number;
  tradelines: ILineInfo[];
}
