import { Injectable } from '@nestjs/common';

import {
  ISummary,
  ILineInfo,
  IExpenses,
} from '../interfaces/tradelines.interface';
import { BaseTradelineService } from './basetradeline.service';

@Injectable()
export class TradelinesService extends BaseTradelineService {
  /**
   * Provide a summary in the ITradelineSummary summary containing of
   * fixed_expenses_before_education and list of ILineInfo tradelines
   * @param tradelines
   * @returns ITradelineSummary
   */
  summarizeTradelinesFromCreditReport(tradelines: string[], filteredDate: string = null): ISummary {
    const tradeLines: ILineInfo[] = [];
    tradelines.forEach(tradeline => {
      const tradeLine = this.parseTradeline(tradeline, filteredDate);
      if (tradeLine) {
        const tradeLineInfo = this.transformTradeline(tradeLine);
        tradeLines.push(tradeLineInfo);
      }
    });
    const expenses: IExpenses = this.getFixedExpensesBeforeEducation(tradeLines);
    return {
      expenses,
      tradelines: tradeLines,
    };
  }
}
