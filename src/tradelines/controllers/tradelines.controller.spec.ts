import { Test, TestingModule } from '@nestjs/testing';
import { TradelinesController } from './tradelines.controller';
import { TradelinesService } from '../services/tradelines.service';
import { ITradelineSummary } from '../interfaces/tradelines.interface';

describe('Tradelines Controller', () => {
  let controller: TradelinesController;
  let service: TradelinesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TradelinesController],
      providers: [TradelinesService],
    }).compile();
    controller = module.get<TradelinesController>(TradelinesController);
    service = module.get<TradelinesService>(TradelinesService);
  });

  it('Controller should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return "valid tradeline summary"', () => {
    const tradelines = [
      '2015-10-10 10 12 $1470.31 $659218.00',
      '2015-10-10 5 1 $431.98 $51028.00',
      '2015-10-09 8 15 $340.12 $21223.20',
      '2015-10-10 10 15 $930.12 $120413.00',
      '2015-10-09 12 5 $150.50 $6421.21',
    ];
    const summary: ITradelineSummary = JSON.parse(
      controller.createTradelineSummary(tradelines),
    );
    // console.log(summary);
    expect(summary.fixed_expenses_before_education).toBe(289105);
    expect(summary.tradelines.length).toBe(5);
  });

  it('should return "default housing expense without mortgage tradelines"', () => {
    const tradelines = [
      '2015-10-10 5 1 $431.98 $51028.00',
      '2015-10-09 8 15 $340.12 $21223.20',
      '2015-10-09 12 5 $150.50 $6421.21',
    ];
    const tradelineSummary: ITradelineSummary = JSON.parse(
      controller.createTradelineSummary(tradelines),
    );
    const summary = service.summarizeTradelinesFromCreditReport(tradelines);
    // console.log(summary);
    const housingExpenses =
      tradelineSummary.fixed_expenses_before_education -
      summary.expenses.non_housing_expenses;

    expect(tradelineSummary.fixed_expenses_before_education).toBe(155162);
    expect(tradelineSummary.tradelines.length).toBe(3);
    expect(housingExpenses).toBe(106100);
  });

  it('should return "tradeline summary including invalid mortgage subcode"', () => {
    const tradelines = [
      '2015-10-10 10 12 $1470.31 $659218.00',
      '2015-10-10 5 1 $431.98 $51028.00',
      '2015-10-09 8 15 $340.12 $21223.20',
      '2015-10-10 10 15 $930.12 $120413.00',
      '2015-10-09 12 5 $150.50 $6421.21',
      '2015-10-09 10 5 $150.50 $6421.21' ,
    ];
    const summary: ITradelineSummary = JSON.parse(
      controller.createTradelineSummary(tradelines),
    );
    // console.log(summary);
    expect(summary.fixed_expenses_before_education).toBe(289105);
    expect(summary.tradelines.length).toBe(6);
  });

  it('should return "tradeline summary to exclude tradelines with incorrect number of entries"', () => {
    const tradelines = [
      '2015-10-10 10 12 $1470.31 $659218.00',
      '2015-10-10 5 1 $431.98 $51028.00',
      '2015-10-09 8 15 $340.12 $21223.20',
      '2015-10-10 10 15 $930.12 $120413.00',
      '2015-10-09 12 5 $150.50 $6421.21',
      '2015-10-09 10 5',
      '2015-10-10 5 1 $431.98 $51028.00 340',

    ];
    const summary: ITradelineSummary = JSON.parse(
      controller.createTradelineSummary(tradelines),
    );
    // console.log(summary);
    expect(summary.fixed_expenses_before_education).toBe(289105);
    expect(summary.tradelines.length).toBe(5);
  });

});
