import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TradelinesModule } from './tradelines/tradelines.module';

@Module({
  imports: [TradelinesModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
